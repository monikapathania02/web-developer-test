import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import './App.css';
import Header from './shared/header';
import Product from './components/product/product';

function App() {
  return (
    <div className="App">
      <Header />
      <Product />
    </div>
  );
}

export default App;
