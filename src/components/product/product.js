import React, { useState, Component } from 'react';
import Select from 'react-select'
import Counter from '../../shared/counter';
import Slider from "react-slick";

const options = [
    { value: 'Continental', label: 'Continental' },
    { value: 'Continental1', label: 'Continen1tal' },
    { value: 'Continental2', label: 'Continental2' }
]
const options1 = [
    { value: 'Popularity', label: 'Popularity' },
    { value: 'New', label: 'New' },
    { value: 'Price', label: 'Price' }
]

function Product() {
    const [items] = useState([1, 2, 3]);

    const settings = {
        dots: false,
        infinite: false,
        speed: 500,
        slidesToShow: 2,
        slidesToScroll: 2,
        initialSlide: 0,
        arrows: false,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    infinite: true,
                }
            },

            {
                breakpoint: 768,
                settings: {
                    centerMode: true,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true,


                }
            },
            {
                breakpoint: 480,
                settings: {
                    centerMode: true,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true,

                }
            }
        ]
    };

    return (
        <div className="container-fluid product-layout">
            <h3 className="panel-title">Recommended Products</h3>
            <div className="filters">
                <div className="row">
                    <div className="col-7 d-flex justify-content-start align-items-center">
                        <span>Selected Filters:</span>
                        <Select
                            closeMenuOnSelect={false}

                            className="select-custom"
                            isClearable="true"
                            options={options}
                        />
                    </div>
                    <div className="col-5 d-flex justify-content-end align-items-center">
                        <span>Sort by:</span>
                        <Select
                            closeMenuOnSelect={false}

                            className="select-custom"
                            isClearable="true"
                            options={options1}
                        />
                    </div>
                </div>
            </div>
            <div className="swipe-row">
                <Slider {...settings}>

                    <div className="swipe-slide">
                        <div className="position-relative mb-30">
                            <p className="bestseller">
                                Premium Bestseller
                    </p>
                            <div className="white-bg p-3">

                                <div className="row">
                                    <div className="col-4 col-lg-3 position-relative">
                                        <div className="pdct d-inline-block position-relative">
                                            <img className="img-fluid" src={require('../../assets/img/product.png')} />
                                            <div className="product-label">
                                                <img className="img-fluid" src={require('../../assets/img/pdct-label.png')} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-8 col-lg-9 detail-product-wrap">
                                        <h3>Continental - Contiecocontact 5</h3>
                                        <div className="detail-product">
                                            <div className="approve">
                                                <img src={require('../../assets/img/logo-approved.svg')} />
                                                <div className="model-pdct">
                                                    <span>205/55 R16 H</span>
                                                    <span>SUV</span>
                                                </div>
                                            </div>
                                            <div className="stock-price-wrap">
                                                <div className="stock-price">
                                                    <div className="stock">
                                                        <span>Stock</span>
                                                        <i class="material-icons greenfont">check_circle</i>
                                                    </div>
                                                    <div className="price">
                                                        <label>Price</label>
                                                        <span>₪340</span>
                                                    </div>
                                                </div>
                                                <div className="total">
                                                    <label>Total</label>
                                                    <span>₪1,360</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="act-btn mt-lg-0 mt-md-2 mt-xl-n5 col-lg-9 offset-lg-3 d-flex justify-content-between align-items-center">
                                        <div className="touch-spin">
                                            <Counter />
                                        </div>
                                        <button type="button" className="btn action-btn">
                                            <span>
                                                <small>+</small>
                                                <img width="24" height="21" src={require('../../assets/img/cart-sharp-black.svg')} />
                                            </span>
                                            <label>Add to Cart</label>
                                        </button>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="swipe-slide">
                        <div className="position-relative mb-30">
                            <p className="bestseller">
                                Economy Bestseller
                    </p>
                            <div className="white-bg p-3">

                                <div className="row">
                                    <div className="col-4 col-lg-3 position-relative">
                                        <div className="pdct d-inline-block position-relative">
                                            <img className="img-fluid" src={require('../../assets/img/product.png')} />
                                            <div className="product-label">
                                                <img className="img-fluid" src={require('../../assets/img/pdct-label.png')} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-8 col-lg-9 detail-product-wrap">
                                        <h3>Continental - Contiecocontact 5</h3>
                                        <div className="detail-product">
                                            <div className="approve">
                                                <img src={require('../../assets/img/logo-approved.svg')} />
                                                <div className="model-pdct">
                                                    <span>205/55 R16 H</span>
                                                    <span>SUV</span>
                                                </div>
                                            </div>
                                            <div className="stock-price-wrap">
                                                <div className="stock-price">
                                                    <div className="stock">
                                                        <span>Stock</span>
                                                        <i class="material-icons greenfont">check_circle</i>
                                                    </div>
                                                    <div className="price">
                                                        <label>Price</label>
                                                        <span>₪340</span>
                                                    </div>
                                                </div>
                                                <div className="total">
                                                    <label>Total</label>
                                                    <span>₪1,360</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="act-btn mt-lg-0 mt-md-2 mt-xl-n5 col-lg-9 offset-lg-3 d-flex justify-content-between align-items-center">
                                        <div className="touch-spin">
                                            <Counter />
                                        </div>
                                        <button type="button" className="btn action-btn">
                                            <span>
                                                <small>+</small>
                                                <img width="24" height="21" src={require('../../assets/img/cart-sharp-black.svg')} />
                                            </span>
                                            <label>Add to Cart</label>
                                        </button>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="swipe-slide">
                        <div className="position-relative mb-30">
                            <p className="bestseller">
                                Premium Bestseller
                    </p>
                            <div className="white-bg p-3">

                                <div className="row">
                                    <div className="col-4 col-lg-3 position-relative">
                                        <div className="pdct d-inline-block position-relative">
                                            <img className="img-fluid" src={require('../../assets/img/product.png')} />
                                            <div className="product-label">
                                                <img className="img-fluid" src={require('../../assets/img/pdct-label.png')} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-8 col-lg-9 detail-product-wrap">
                                        <h3>Continental - Contiecocontact 5</h3>
                                        <div className="detail-product">
                                            <div className="approve">
                                                <img src={require('../../assets/img/logo-approved.svg')} />
                                                <div className="model-pdct">
                                                    <span>205/55 R16 H</span>
                                                    <span>SUV</span>
                                                </div>
                                            </div>
                                            <div className="stock-price-wrap">
                                                <div className="stock-price">
                                                    <div className="stock">
                                                        <span>Stock</span>
                                                        <i class="material-icons greenfont">check_circle</i>
                                                    </div>
                                                    <div className="price">
                                                        <label>Price</label>
                                                        <span>₪340</span>
                                                    </div>
                                                </div>
                                                <div className="total">
                                                    <label>Total</label>
                                                    <span>₪1,360</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="act-btn mt-lg-0 mt-md-2 mt-xl-n5 col-lg-9 offset-lg-3 d-flex justify-content-between align-items-center">
                                        <div className="touch-spin">
                                            <Counter />
                                        </div>
                                        <button type="button" className="btn action-btn">
                                            <span>
                                                <small>+</small>
                                                <img width="24" height="21" src={require('../../assets/img/cart-sharp-black.svg')} />
                                            </span>
                                            <label>Add to Cart</label>
                                        </button>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="swipe-slide">
                        <div className="position-relative mb-30">
                            <p className="bestseller">
                                Economy Bestseller
                    </p>
                            <div className="white-bg p-3">

                                <div className="row">
                                    <div className="col-4 col-lg-3 position-relative">
                                        <div className="pdct d-inline-block position-relative">
                                            <img className="img-fluid" src={require('../../assets/img/product.png')} />
                                            <div className="product-label">
                                                <img className="img-fluid" src={require('../../assets/img/pdct-label.png')} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-8 col-lg-9 detail-product-wrap">
                                        <h3>Continental - Contiecocontact 5</h3>
                                        <div className="detail-product">
                                            <div className="approve">
                                                <img src={require('../../assets/img/logo-approved.svg')} />
                                                <div className="model-pdct">
                                                    <span>205/55 R16 H</span>
                                                    <span>SUV</span>
                                                </div>
                                            </div>
                                            <div className="stock-price-wrap">
                                                <div className="stock-price">
                                                    <div className="stock">
                                                        <span>Stock</span>
                                                        <i class="material-icons greenfont">check_circle</i>
                                                    </div>
                                                    <div className="price">
                                                        <label>Price</label>
                                                        <span>₪340</span>
                                                    </div>
                                                </div>
                                                <div className="total">
                                                    <label>Total</label>
                                                    <span>₪1,360</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="act-btn mt-lg-0 mt-md-2 mt-xl-n5 col-lg-9 offset-lg-3 d-flex justify-content-between align-items-center">
                                        <div className="touch-spin">
                                            <Counter />
                                        </div>
                                        <button type="button" className="btn action-btn">
                                            <span>
                                                <small>+</small>
                                                <img width="24" height="21" src={require('../../assets/img/cart-sharp-black.svg')} />
                                            </span>
                                            <label>Add to Cart</label>
                                        </button>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>



                </Slider>


            </div>

            <div className="row">
                {items.map((item) => {
                    return (

                        <div className="col-lg-12 col-md-6">
                            <div className="white-bg product-list" key={item}>
                                <div className="sm-view">
                                    <h4>Continental PremiumContact™ 6</h4>
                                    <img src={require('../../assets/img/logo-approved.svg')} />
                                    <div className="model-pdct d-none d-lg-flex">
                                        <span>205/55 R16 H</span>
                                        <span>SUV</span>
                                    </div>
                                    <div className="stock">
                                        <span>Stock <i className={`material-icons ${item == 2 ? 'yellowfont' : item == 3 ? 'redfont' : 'greenfont'}`}>check_circle</i></span>
                                        <small className={item == 3 ? 'redfont' : ''}>{item == 2 ? '8 Tires Left' : item == 3 ? 'Back in 1 week!' : ''} </small>
                                    </div>
                                    <div className="touch-spin d-lg-none">
                                        <Counter />
                                    </div>
                                </div>


                                <div className="sm-view">
                                    <div className="model-pdct d-lg-none">
                                        <span>205/55 R16 H</span>
                                        <span>SUV</span>
                                    </div>
                                    <div className="price ">
                                        <label>Price</label>
                                        <span>₪340</span>
                                    </div>
                                    <div className="touch-spin d-none d-lg-flex">
                                        <Counter />
                                    </div>
                                    <div className="total">
                                        <p><label>Total</label>
                                            <span>₪1,360</span></p>
                                        <small className="redfont d-lg-none">Save ₪408!</small>
                                    </div>
                                    <div className="list-btn">
                                        {item == 3 ?
                                            <button type="button" className="btn action-btn noty-btn">
                                                <span>

                                                    <img width="16" height="20" src={require('../../assets/img/notifications-sharp.svg')} />
                                                </span>
                                                <label>Notify Me!</label>
                                            </button>
                                            :
                                            <button type="button" className="btn action-btn">
                                                <span>
                                                    <small>+</small>
                                                    <img width="24" height="21" src={require('../../assets/img/cart-sharp-black.svg')} />
                                                </span>
                                                <label>Add to Cart</label>
                                            </button>
                                        }
                                    </div>
                                </div>

                            </div>
                        </div>
                    )
                })
                }
            </div>
            <div className="row">
                <div className="col-12">
                    <button type="button" className="btn load-more">Load More Results</button>
                </div>
            </div>

        </div>


    );
}

export default Product;