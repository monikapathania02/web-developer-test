import React, { useState } from 'react';


function Counter() {
    const [counter, setcounters] = useState((1));

    const countminus = () => {
        if (counter > 1)
            setcounters(counter - 1)
    };
    const countplus = () => {
        if (counter < 4)
            setcounters(counter + 1)
    }
    return (
        <React.Fragment>
            <button onClick={countminus} type="button"><i class="material-icons">remove</i></button>
            <span>{counter}</span>
            <button onClick={countplus} countplus type="button"><i class="material-icons">add</i></button>
        </React.Fragment>
    );
}

export default Counter;
