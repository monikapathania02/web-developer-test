import React from 'react';

function Header() {
  return (

    <header>
      <nav>
        <button type="text" className="btn p-0">
          <img width="26" height="22" src={require('../assets/img/options-sharp.svg')} />
        </button>
      </nav>
      <div className="search-box">
        <form className="form-inline">
          <input className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" />
          <button className="btn" type="submit"><span className="material-icons">search</span></button>
        </form>
      </div>
      <div className="header-links">
        <div className="user-link">
          <a href="#">
            <img width="24" height="25" src={require('../assets/img/person-sharp.svg')} />
            <label>Account</label>
          </a>
        </div>
        <div className="user-link">
          <a href="#">
            <img width="20" height="25" src={require('../assets/img/document-text-sharp.svg')} />
            <label>Orders</label>
          </a>
        </div>
        <div className="user-link">
          <a className="position-relative" href="#">
            <span></span>
            <img width="30" height="25" src={require('../assets/img/cart-sharp.svg')} />
            <label>Cart</label>
          </a>
        </div>
      </div>

    </header>
  );
}

export default Header;



